<?php
    if(! function_exists('curl_post'))
    {
        function curl_post($base_url = '', $post_data = '')
        {
            $content_type   = 'Content-Type: application/x-www-form-urlencoded';
            $accept         = 'Accept: application/json';
            $authorization  = 'Authorization: Bearer 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';

            $ch             = curl_init();

            curl_setopt($ch, CURLOPT_URL, $base_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($content_type, $accept, $authorization));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

            $resp           = curl_exec($ch);
            $response       = json_decode($resp, true);

            return $response;
        }
    }

    $base_url               = 'https://api.sendgrid.com/api/mail.send.json';

    $name                   = (isset($_POST['name'])) ? trim($_POST['name']) : '';
    $email                  = (isset($_POST['email'])) ? trim($_POST['email']) : '';
    $subject                = (isset($_POST['subject'])) ? trim($_POST['subject']) : '';
    $message                = (isset($_POST['message'])) ? trim($_POST['message']) : '';

    if($name == '' OR $email == '' OR $subject == '' OR $message == '')
        header('Location: http://www.tmoney.co.id/');

    $post_data['to']        = 'customerservice@tmoney.co.id';
    $post_data['from']      = $email;
    $post_data['subject']   = $subject;
    $post_data['html']      = '<strong>' . $message . '</strong>';

    $result                 = json_encode(curl_post($base_url, $post_data));

    echo $result;

    header('Location: http://www.tmoney.co.id/');
?>